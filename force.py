# -*- coding: utf-8 -*-
"""
Created on Wed Feb 20 14:43:17 2019

@author: Chris Flowers
"""

from vector2 import vector2

class SingleForce:
    def __init__ (self, objects, force_function):
        self.objects = objects
        self.force_function = force_function
        for o in objects:
            o.interactions.append(self)
            
    def force_all(self):
        for obj in self.objects:
            obj.force += self.force_function(obj)
            
    def remove(self, obj):
        self.objects.remove(obj)
        
    def delete(self):
        for o in self.objects:
            o.interactions.remove(self)
        self.objects.clear()
    
#spring_force(objects[i], objects[i], springConstant, naturalLength)
def spring_force(obj1, obj2, k, natLength):
    #finPos and initPos have to be vector2
    l = obj2.pos - obj1.pos
    return -k*(l.mag() - natLength)*l.hat()



class PairForce:
    def __init__(self, objects, force_function):
        self.objects = objects
        self.force_function = force_function
        for o in objects:
            o.interactions.append(self)
    
    def force_all(self):
        # Loop through all pairs once
        for i in range(1, len(self.objects)):
            obj1 = self.objects[i]
            for j in range(i):
                obj2 = self.objects[j]
                force = self.force_function(obj1, obj2)
                obj1.force += force
                obj2.force -= force

    def remove(self, obj):
        self.objects.remove(obj)   

    def draw(screen):
        pass 

def linear_drag(obj, coeff):
    return -obj.vel*coeff    

def gravity_force(obj1, obj2, G):
    r = obj1.pos - obj2.pos
    rmag = r.mag()
    print(rmag)
    if rmag > obj1.radius + obj2.radius:
        return -G/(obj1.invmass*obj2.invmass*r.mag2())*r.hat()
    else:
        return vector2(0,0)