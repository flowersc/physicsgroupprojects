# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 14:37:01 2019

@author: Chris Flowers
"""

from vector2 import vector2
from Circle import Circle
from math import sqrt
from Wall import Wall
from Polygon import Polygon
from UniformPolygon import UniformPolygon
import force

class Contact:
    def __init__(self, obj1, obj2, data, normal, penetration, pos=None, wall_pos=None):
        self.obj1 = obj1
        self.obj2 = obj2
        self.data = data
        self.normal = normal
        self.penetration = penetration
        self.pos = pos
        self.wall_pos = wall_pos
        
    def resolve(self):
        if self.pos is not None and self.wall_pos is not None:
            self.penetration = (self.wall_pos - self.pos)*self.normal
        restitution = self.data['restitution']
        friction = self.data['friction']
        static = self.data['static']
        
        # Resolve penetration
        if self.penetration > 0:
            total_invmass = self.obj1.invmass + self.obj2.invmass
            if total_invmass > 0:
                nudge = self.penetration/total_invmass
                self.obj1.translate(self.obj1.invmass*nudge*self.normal)
                self.obj2.translate(-self.obj2.invmass*nudge*self.normal)
                
        # Resolve velocity
        n = self.normal
        t = n.perp()
                
        #S1 = rcontact - r1
        S1 = self.pos - self.obj1.pos
        #S2 = rcontact - r2
        S2 = self.pos - self.obj2.pos
        vel1 = vector2(self.obj1.vel)
        ''' Modify vel1 and vel2 or sep_vel to take into account rotation.  
            sep_vel refers to the relative velocity at the point of contact.
            Use if hasattr(self.obj1, "angvel") to check if rotatable.'''
        if hasattr(self.obj1, "angvel"):
            S1 = self.pos - self.obj1.pos
            vel1 += self.obj1.angvel % S1
            
        vel2 = vector2(self.obj2.vel)
        if hasattr(self.obj2, "angvel"):
            S2 = self.pos - self.obj2.pos
            vel2 += self.obj2.angvel % S2
                    
        sep_vel = (vel1 - vel2)*self.normal
        if sep_vel < 0: # if moving toward one another
            if static == 0: 
                # Collisions WITHOUT friction
                target_sep_vel = -restitution*sep_vel
                delta_vel = target_sep_vel - sep_vel
                ''' Add to total_invmass those extra terms involving rotation:
                    invmomi * (s % normal)**2 
                    Define s1 and s2 for use here and later.
                    Use if hasattr(self.obj, "invmomi") to check if rotatable.'''
                total_invmass = self.obj1.invmass + self.obj2.invmass 
                print(total_invmass)
                print('FIRST TOT INV MASS ^')
                if hasattr(self.obj1, "invmomi"):
                    sn = S1 % self.normal
                    #S1t = S1*n.hat()
                    total_invmass += self.obj1.invmomi*(sn*sn)
                if hasattr(self.obj2, "invmomi"):
                    sn = S2 % self.normal
                    #S2t = S2*t.hat()
                    total_invmass += self.obj2.invmomi*(sn*sn)
                    
                
                
                Wtt = total_invmass
                total_invmass = total_invmass * -1
                if total_invmass > 0:
                    total_invmass = -1*total_invmass
                    impulse = delta_vel/total_invmass*self.normal
                    print('total impulse |')
                    print(impulse)
                    self.obj1.add_impulse(impulse, self.pos)
                    self.obj2.add_impulse(impulse, self.pos)
                    return impulse # returns impulse for visualization purposes
            else: 
                
                #S1 = rcontact - r1
                S1 = self.pos - self.obj1.pos
                #S2 = rcontact - r2
                S2 = self.pos - self.obj2.pos
                
                # Collisions WITH friction
                # Abbreviations for normal and tangential
                n = self.normal
                t = n.perp()

                # Initial relative velocity and normal and tangential components
                vi = vel1 - vel2
                vin = vi*n
                vit = vi*t
                
                # This gets started the matrix W
                total_invmass = self.obj1.invmass + self.obj2.invmass               
                Wtt = total_invmass
                Wnn = total_invmass
                Wnt = 0
                ''' (1) Replace 0s with correct values.  
                    Define s1n, s1t, s2n, s2t, to simplify the expressions.'''
                #S1n = S1 dot n.hat
                S1n = S1*n.hat()
                #S1t = S1 dot t.hat
                S1t = S1*t.hat()
                S2n = S2.hat()
                S2t = S2*t.hat()
                if hasattr(self.obj1, "invmomi"):
                    Wtt += self.obj1.invmomi*(S1t*S1t)
                    Wnn += self.obj1.invmomi*(S1n*S1n)
                    Wnt += self.obj1.invmomi*S1n*S1t
                if hasattr(self.obj2, "invmomi"):
                    Wtt += self.obj2.invmomi*(S2t*S2t)
                    Wnn += self.obj2.invmomi*(S2n*S2n)
                    Wnt += self.obj2.invmomi*(S2n*S2t)
                
                # Target velocity change for perfect static friction
                dvn = -(1 + restitution)*vin
                dvt = -vit
                
                ''' (2) Compute the normal component of impulse, Jn, needed 
                    to achieve the normal velocity change, dvn. '''
                # compute Jn here?
                # Jn = [WnnGradVn + WntGradVt]/det(WnnWtt - Wnt^2)
                Jn = (Wnn*dvn + Wnt*dvt) / (Wnn*Wtt - (Wnt * Wnt))
                
                ''' (3) Compute mu as friction coefficient needed 
                    to achieve the required dvt, where Jt = mu*Jn. '''
                Qn = Wnn*dvn + Wnt*dvt
                Qt = (Wnt*dvn + Wtt*dvt)
                mu = Qt/Qn
                
                ''' (4) If mu is too large for the friction coefficients, 
                    reduce it to an acceptable value. '''
                if (mu > static):
                    mu = friction
                if (mu < -1*static):
                    mu = -1*friction

                ''' (5) Compute impulse as the sum of normal and tangential 
                    components, Jn and Jt. '''
                impulse = Jn*n.hat() + mu*Jn*n.hat()
                
                self.obj1.add_impulse(impulse, self.pos)
                self.obj2.add_impulse(-impulse, self.pos)
                return impulse # returned so that it can be visualized
                
        
    def hole(self, forces):
        #print("In hole")
        for f in forces:
            f.force_all()
        #forces.append(force.gravity_force(self.obj1, self.obj2, 10000))
        #force.spring_force(self.obj1, self.obj2, 0.1, 10)
        
    def hillSlide(self):
        hNormal = vector2(0, -1)
        #print(-1*(self.obj1.vel)* hNormal * 5)
        self.obj1.vel.x += (1*self.obj1.vel.x) * hNormal.x * 1/80
        self.obj1.vel.y += hNormal.y * 5
        #self.obj1.vel += -1*(self.obj1.vel) * hNormal * 5
        #IF 0 DO SOMETHING: NOT SURE
        
    def friction(self):
        #print("slow down!!!!")
        self.obj1.vel += force.linear_drag(self.obj1, 1/10)
        
    def resolve_penetration(self):
        if self.penetration > 0:
            total_invmass = self.obj1.invmass + self.obj2.invmass
            nudge = self.penetration/total_invmass
            self.obj1.pos += (self.obj1.invmass*nudge)*self.normal
            self.obj2.pos -= (self.obj2.invmass*nudge)*self.normal

    def resolve_velocity(self):
        vel1 = vector2(self.obj1.vel)
        if hasattr(self.obj1, "angvel"):
            s1 = self.pos - self.obj1.pos
            vel1 += self.obj1.angvel % s1
        vel2 = vector2(self.obj2.vel)
        if hasattr(self.obj2, "angvel"):
            s2 = self.pos - self.obj2.pos
            vel2 += self.obj2.angvel % s2
        sep_vel = (vel1 - vel2)*self.normal
        #print("SEP:", sep_vel)
        if sep_vel < 0:
            target_sep_vel = -self.restitution*sep_vel
            delta_vel = target_sep_vel - sep_vel
            total_invmass = self.obj1.invmass + self.obj2.invmass
    
            if hasattr(self.obj1, "invmomi"):
                sn = s1 % self.normal
                total_invmass += self.obj1.invmomi*sn*sn
            if hasattr(self.obj2, "invmomi"):
                sn = s2 % self.normal
                total_invmass += self.obj1.invmomi*sn*sn
              
            if total_invmass > 0:
                impulse = delta_vel/total_invmass
                self.obj1.vel += (self.obj1.invmass*impulse)*self.normal
                self.obj2.vel -= (self.obj2.invmass*impulse)*self.normal

class ContactGenerator:
    def __init__(self, objects, restitution=None, friction=None, static=None):
        self.objects = objects
        if restitution is None:
            restitution = 0
            print("Restitution not given.  Assuming zero restitution.")
        if friction is None:
            friction = 0
            print("Kinetic friction coefficient not given.  Assuming zero.")
        if static is None:
            static = friction
            print("Static friction coefficient not given.  Assuming the same as kinetic.")
        else:
            if static < friction:
                print("Warning, static friction coefficient must not be less than kinetic; setting it equal to kinetic.")
                static = friction
        self.data = {'restitution': restitution,
                     'friction': friction,
                     'static': static}
        
        for o in objects:
            o.contacts.append(self)
        self.contacts = []
    
    def remove(self, obj):
        self.objects.remove(obj)
        
    def contact_all(self):
        self.contacts.clear()
        # Loop through all pairs once
        for i in range(1, len(self.objects)):
            obj1 = self.objects[i]
            for j in range(i):
                obj2 = self.objects[j]
                if isinstance(obj1, Circle) and isinstance(obj2, Circle):
                    self.contacts.extend(contact_circle_circle(obj1, obj2, self.data))
                elif isinstance(obj1, Circle) and isinstance(obj2, Wall):
                    self.contacts.extend(contact_wall_circle(obj2, obj1, self.data))
                elif isinstance(obj1, Wall) and isinstance(obj2, Circle):
                    print(len(self.contacts))
                    self.contacts.extend(contact_wall_circle(obj1, obj2, self.data))
                elif isinstance(obj1, Wall) and isinstance(obj2, Wall):
                    pass
                elif isinstance(obj1, Circle) and isinstance(obj2, Polygon):
                    self.contacts.extend(contact_circle_polygon(obj1, obj2, self.data))
                elif isinstance(obj1, Polygon) and isinstance(obj2, Circle):
                    self.contacts.extend(contact_circle_polygon(obj2, obj1, self.data))
                elif isinstance(obj1, Polygon) and isinstance(obj2, Polygon):
                    self.contacts.extend(contact_poly_poly(obj1, obj2, self.data))
                elif isinstance(obj1, UniformPolygon) and isinstance(obj2, UniformPolygon):
                    self.contacts.extend(contact_poly_poly(obj1, obj2, self.data))
                elif isinstance(obj1, Polygon) and isinstance(obj2, Wall):
                    print("POLY WALL")
                    self.contacts.extend(contact_polygon_wall(obj1, obj2, self.data))
                elif isinstance(obj1, Wall) and isinstance(obj2, Polygon):
                    print("POLLY WALLLLLLL")
                    self.contacts.extend(contact_polygon_wall(obj2, obj1, self.data))
                elif isinstance(obj1, UniformPolygon) and isinstance(obj2, Wall):
                    print("POLY WALL")
                    self.contacts.extend(contact_polygon_wall(obj1, obj2, self.data))
                elif isinstance(obj1, Wall) and isinstance(obj2, UniformPolygon):
                    print("POLLY WALLLLLLL")
                    self.contacts.extend(contact_polygon_wall(obj2, obj1, self.data))
                    '''
                else:
                    print("Warning! ContactGenerator not implemented between ",
                          type(obj1)," and ", type(obj2),".")
                    '''
        return self.contacts

def contact_circle_circle(obj1, obj2, data):
    r = obj1.pos - obj2.pos
    rmag2 = r.mag2()
    R = obj1.radius + obj2.radius
    if rmag2 < R*R:
        rmag = sqrt(rmag2)
        penetration = R - rmag
        normal = r/rmag
        return [Contact(obj1, obj2, data, normal, penetration)]
    return []
        #self.contacts.append(Contact(obj1, obj2, self.restitution, normal, penetration))
        #print("Here?")
    
def contact_wall_circle(wall, circle, data):
    #print("inside the contact_wall_circle")
    rc = circle.pos * wall.normal
    rw = wall.pos * wall.normal
    penetration = circle.radius + (rc - rw)
    if (rc - rw) < circle.radius:
        return [Contact(wall, circle, data, -1* wall.normal, penetration/10)]
        #self.contacts.append(Contact(wall, circle, self.restitution, -1* wall.normal, penetration/10))
            
def contact_circle_polygon(circle, poly, data):
    #print("In the contact")
    least_p = 10000000000000000
    least_d2 = 10000000000000000
    """
    for i in range(len(poly.points)):
        print(poly.points[i])
    """
    for i in range(len(poly.points)):
        #print("Inside the given loop: ", poly.points[i])
        #print("POINTS", poly.points[i])
        #print("CIRCLE POS", circle.pos)
        p = (poly.points[i] - circle.pos) * poly.normals[i] + circle.radius  
        if p <= 0:
            #print("return")
            #print(poly.points[i], p)
            return []
        if p < least_p:
            least_p = p
            least_n = poly.normals[i]
    for r in poly.points:
        d2 = (r - circle.pos).mag2()
        if d2 < least_d2:
            least_d2 = d2
            closest_point = r
    normal = (closest_point - circle.pos).hat()
    wall_pos = circle.pos + circle.radius*normal
    max_p = -100000000000000000
    for r in poly.points:
        p = (wall_pos - r)*normal
        if p > max_p:
            max_p = p
    if max_p <= 0:
        return []
    if max_p < least_p:
        #print("first contact")
        return [Contact(poly, circle, data, normal, max_p)]
        #self.contacts.append(Contact(poly, circle, 1, normal, max_p))
    else:
        #print("second contact")
        return [Contact(circle, poly, data, least_n, least_p)]
        #self.contacts.append(Contact(circle, poly, 1, least_n, least_p))
        
        
def contact_poly_poly(poly1, poly2, data):
    least_p = float("inf")
    for i in range(len(poly1.points)):
        max_p = -float("inf")
        for r in poly2.points:
            p = (poly1.points[i] - r)*poly1.normals[i]
            if p > max_p:
                max_p = p
                max_r = r
        if max_p < 0:
            return []
        if max_p < least_p:
            least_p = max_p
            contact = Contact(poly1, poly2, data, poly1.normals[i], least_p, max_r)
            
    for i in range(len(poly2.points)):
        max_p = -float("inf")
        for r in poly1.points:
            p = (poly2.points[i] - r)*poly2.normals[i]
            if p > max_p:
                max_p = p
                max_r = r
        if max_p < 0:
            return []
        if max_p < least_p:
            least_p = max_p
            contact = Contact(poly2, poly1, data, poly2.normals[i], least_p, max_r)
    return [contact]

def contact_polygon_wall(poly, wall, data):
    max_p = -float('inf')
    ''' Find which point of the polygon give the maximum penetration.
        Store the point as max_r and the penetration as max_p. '''
    for r in poly.points:
        p = (wall.pos - r) * (wall.normal)
        if p > max_p:
            max_p = p
            max_r = r
        if max_p <= 1e-32:
            return []
            
    ''' If max_p is less than 0, then there is no collision; 
        return an empty list. '''
    
    # Otherwise, return the following. (Done for you.)
    return [Contact(poly, wall, data, wall.normal, max_p, max_r, wall.pos)]
            
        
    
                
        
        
            

