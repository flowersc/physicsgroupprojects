# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 11:33:24 2019

@author: Chris Flowers
"""

from Particle import Particle
from vector2 import vector2
import pygame

class Wall(Particle):
    def __init__(self, pos, normal, color=(0,0,0), length=1000):
        Particle.__init__(self, 0, pos)
        self.normal = normal.hat()
        self.color = color
        self.length = length

    def draw(self, screen):
        tangent = self.normal.perp()
        p1 = (self.pos + self.length*tangent).pygame()
        p2 = (self.pos - self.length*tangent).pygame()
        pygame.draw.line(screen, self.color, p1, p2)
        
        

