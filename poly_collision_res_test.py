# -*- coding: utf-8 -*-
"""
Created on Fri Apr 12 14:33:16 2019

@author: snarlsburg
"""

import pygame
from random import randint, uniform
from math import pi
from UniformPolygon import UniformPolygon
from Wall import Wall
from vector2 import vector2
import contact
import colorsys

WHITE = (255,255,255)
BLACK = (0,0,0)


def random_hsv_color(hlo, hhi, slo, shi, vlo, vhi):
    if hhi <  hlo:
        hhi += 1
    h = uniform(hlo, hhi)%1
    s = uniform(slo, shi)
    v = uniform(vlo, vhi)
    r, g, b = colorsys.hsv_to_rgb(h,s,v)
    return int(255*r), int(255*g), int(255*b)


def main():
    pygame.init()
    screen = pygame.display.set_mode([800,600])
    
    gravity = vector2(0,200) # Downward uniform gravity, set to zero
    
    # list of objects in world
    objects = []
    # list of forces active    
    forces = []
    # list of contact generators
    contact_generators = []
    # list of contacts to be resolved
    contacts = []
    
    #objects.append(Circle(9, vector2(400-30,300), vector2(0,1), 60, (255,127,0), gravity))
    for n in range(3):
        
        objects.append(UniformPolygon(1e-4, vector2(randint(100,700),randint(0,500)), 
                                      (0,0), uniform(-pi,pi), 0, 
                                      [(randint(-10,10), randint(160,180)),
                                       (randint(-50,-30), randint(-10,10)),
                                       (randint(30,50), randint(-10,10))],
                                      BLACK, gravity))
        
        
        #objects.append(UniformPolygon(1e-4, vector2(randint(100, 700),randint(0, 500)), (0,0), uniform(-pi, pi), 0, ((randint(-10, 10), randint(160,180)), (randint(-50, -30), randint(-10, 10)), (randint(30, 50), randint(-10, 10))), (255, 0, 0), gravity))
    
    #print(1/objects[0].invmass, 1/objects[0].invmomi)  
    
    objects.append(Wall(vector2(0,600), vector2(0,-1)))
    objects.append(Wall(vector2(200,400), vector2(1,-1)))
    objects.append(Wall(vector2(600,500), vector2(-1,-2)))
    contact_generators.append(contact.ContactGenerator(objects))

    # Main Loop
    done = False
    frame_rate = 60
    dt = 1.0/frame_rate

    clock = pygame.time.Clock()
    while not done:
        # --- Main event loop
        for event in pygame.event.get(): # User did something
            if (event.type == pygame.QUIT # If user clicked close
                or (event.type == pygame.KEYDOWN 
                    and event.key == pygame.K_ESCAPE)): # or pressed ESC
                done = True # Flag that we are done so we exit this loop
        
        # Add forces
        for f in forces:
            f.force_all()
        
        # Move objects
        for o in objects:
            o.integrate(dt)
        
        # Get contacts
        niterations = 0
        max_iterations = 1
        while niterations < max_iterations:
            niterations += 1
            contacts = []
            for g in contact_generators:
                contacts.extend(g.contact_all())
              
            if len(contacts)==0:
                break
                
            # Resolve contacts
            contacts.sort(key=lambda x: x.penetration)
            for c in contacts:
                c.resolve()

        # Draw objects to screen
        screen.fill(WHITE) # clears the screen
        for o in objects:
            #print(o.pos)
            o.draw(screen)
        
        # Update the screen
        pygame.display.update()

        # --- Limit to 60 frames per second
        clock.tick(60)
        
    pygame.quit()
 
""" Safe start """
if __name__ == "__main__":
    try:
        main()
    except:
        pygame.quit()
        raise