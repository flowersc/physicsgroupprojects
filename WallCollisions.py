# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 11:55:16 2019

@author: Chris Flowers
"""


from Wall import Wall
import pygame
from Circle import Circle
from vector2 import vector2
import contact
import force
from Polygon import Polygon

WHITE = (255,255,255)
BLACK = (0,0,0)

objects = []
walls = []
sTraps = []
hills = []
holes = []

forces = []
circles = []

points = []
hillPoints = []
sandPoints = []
wallPoints = []

pNormals = []
sNormals = []
hNormals = []
wNormals = []

velocity_vector = vector2(0,0)
maxRange = 984.6024578478361
#power_pos = vector2(0,0)


def main():
    #initialize pygame and the font used for the power.
    pygame.init()
    pygame.font.init()
    myFont = pygame.font.SysFont('Comic Sans MS', 30)
    #-------------------
    
    screen = pygame.display.set_mode([800,600])
    #screen.fill(BLACK)
    
    
    gravity = vector2(0,0) # Downward uniform gravity, set to zero
    G = 0.000009 # Gravity between objects

    # Main Loop
    done = False
    frame_rate = 60
    dt = 1.0/frame_rate

    clock = pygame.time.Clock()
    
    #Game Boundaries
    bottom_wall = Wall(pos = vector2(400, 600), normal = vector2(0,-1), color = BLACK, length = 1)
    top_wall = Wall(pos = vector2(400, 0), normal = vector2(0, 1), color = BLACK, length = 1)
    left_wall = Wall(pos = vector2(0, 300), normal = vector2(1, 0), color = BLACK, length = 1)
    right_wall = Wall(pos = vector2(800, 600), normal = vector2(-1, 0), color = BLACK, length = 1)
    print(bottom_wall)
    print('bottom wall ^')
    print(top_wall)
    print('top wall ^')
    print(left_wall)
    print('left wall ^')
    print(right_wall)
    print('right wall ^')
    #---------------
    
    #points for polygon
    p1 = vector2(575, 475)
    p2 = vector2(625, 475)
    p3 = vector2(550, 600)
    p4 = vector2(600, 600)
    points.append(p1)
    points.append(p2)
    points.append(p4)
    points.append(p3)
    #---------------
    
    #points for wall
    w1 = vector2(500, 200)
    w2 = vector2(550, 200)
    w3 = vector2(550, 600)
    w4 = vector2(500, 600)
    wallPoints.append(w1)
    wallPoints.append(w2)
    wallPoints.append(w3)
    wallPoints.append(w4)
    #---------------
    
    #points for sandtrap
    #make sure it is clockwise
    s1 = vector2(550, 100)
    s2 = vector2(600, 75)
    s3 = vector2(650, 100)
    s4 = vector2(650, 175)
    s5 = vector2(600, 200)
    s6 = vector2(550, 175)
    sandPoints.append(s1)
    sandPoints.append(s2)
    sandPoints.append(s3)
    sandPoints.append(s4)
    sandPoints.append(s5)
    sandPoints.append(s6)
    #----------------
    
    #polygon for hill and a hNormal in the direction of the hill slope.
    h1 = vector2(600, 425)
    h2 = vector2(800, 425)
    h3 = vector2(800, 475)
    h4 = vector2(625, 475)
    hillPoints.append(h1)
    hillPoints.append(h2)
    hillPoints.append(h3)
    hillPoints.append(h4)
    #----------------
    
    #hole for the level
    golfHole = Circle(0, (700, 550), (0,0), 0.9, (255, 0, 0), gravity)
    golfHoleVis = Circle(1000000000, (700, 550), (0,0), 20, (255, 0, 0), gravity)
    golfHoleBowl = Circle(1000000000, (700, 550), (0,0), 30, (255, 0, 255), gravity)
    gravityField = Circle(1000000000, (700, 550), (0,0), 30, (255, 255, 255), gravity)
    #----------------
    
    hill1 = Polygon((0,0), hillPoints, (0, 255, 0), width = 1)
    sandtrap = Polygon((0,0), sandPoints, (244, 164, 96), width = 1)
    poly1 = Polygon((0,0), points, (0, 255, 255), width = 1)
    polyWall = Polygon((0,0), wallPoints, (255, 255, 0), width = 1)
    #testWall = Wall(1, BLACK, pos = vector2(425, 500), normal = vector2(-0.9701425001453319, -0.24253562503633297))
    #testWall2 = Wall(1, BLACK, pos = vector2(425, 500), normal = vector2(0, 1))
    golfBall = Circle(10, (700, 500), vector2(0, 0), 10, (0,0,255), gravity)
    
    forces.append(force.PairForce(circles, lambda o1, o2: force.spring_force(o1, o2, -100, 1)))
    
    #obj1 = Circle(0, (50, 550), (0,0), 10, (255, 0, 0), gravity)
    objects.append(golfBall)
    hills.append(golfBall)
    hills.append(hill1)
    sTraps.append(golfBall)
    sTraps.append(sandtrap)
    objects.append(poly1)
    objects.append(polyWall)
    '''
    walls.append(golfBall)
    walls.append(bottom_wall)
    walls.append(top_wall)
    walls.append(left_wall)
    walls.append(right_wall)
    '''
    circles.append(golfHole)
    circles.append(golfBall)
    holes.append(gravityField)
    holes.append(golfBall)
    
    hole_generator = contact.ContactGenerator(holes, 1)
    hill_generator = contact.ContactGenerator(hills, 1)
    contact_generator = contact.ContactGenerator(objects)
    #wall_contact_generator = contact.ContactGenerator(walls, friction = 0)
    sand_trap_c_gen = contact.ContactGenerator(sTraps, 1)
    mouse_pos = golfBall.pos
    power = myFont.render('Power: ', False, (0,0,0))
    
    
    while not done:
        # --- Main event loop
        length_velocity_vector = (vector2(pygame.mouse.get_pos()) - golfBall.pos).mag()
        velocity_vector = vector2(mouse_pos - golfBall.pos)
        percentage_power = length_velocity_vector/(maxRange/3)
        #print(percentage_power)
    
        for event in pygame.event.get(): # User did something
            if (event.type == pygame.QUIT # If user clicked close
                or (event.type == pygame.KEYDOWN 
                    and event.key == pygame.K_ESCAPE)): # or pressed ESC
                done = True # Flag that we are done so we exit this loop
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                #this allows the user to apply a force in the direction of the velocity
                #vector on the golfBall.
                if golfBall.vel.mag() == 0:
                    golfBall.vel += velocity_vector * 2
                
        #friction on grass.
        if golfBall.vel.mag() > 0:
            golfBall.vel += force.linear_drag(golfBall, 1/100)
        if golfBall.vel.mag() < 1:
            golfBall.vel = vector2(0,0)
            
                
                
        screen.fill(WHITE)
        """
        Draw objects to the screen.
        """
        for i in range(len(walls)):
            walls[i].draw(screen)
        polyWall.draw(screen)
        gravityField.draw(screen)
        golfHoleBowl.draw(screen)
        golfHole.draw(screen)
        golfHoleVis.draw(screen)
        sandtrap.draw(screen)
        hill1.draw(screen)
        golfBall.draw(screen)
        #obj1.draw(screen)
        poly1.draw(screen)
        #testWall.draw(screen)
        #testWall2.draw(screen)
        golfBall.integrate(dt)
        hill_generator.contact_all()
        contact_generator.contact_all()
        hole_generator.contact_all()
        #wall_contact_generator.contact_all()
        sand_trap_c_gen.contact_all()
         
        #contact generator for circles
        for i in range (len(contact_generator.contacts)):
            contact_generator.contacts[i].resolve()
        
        #contact generator for walls
        #for i in range (len(wall_contact_generator.contacts)):
            #wall_contact_generator.contacts[i].resolve()
        
        for i in range (len(sand_trap_c_gen.contacts)):
            sand_trap_c_gen.contacts[i].friction()
            
        for i in range (len(hill_generator.contacts)):
            hill_generator.contacts[i].hillSlide()
            
        for i in range (len(hole_generator.contacts)):
            hole_generator.contacts[i].hole(forces)
            for c in circles:
                c.integrate(dt)
            #for f in forces:
                #f.force_all()
        
        """
        Debug
        """
        if golfBall.vel.mag() == 0:
            if percentage_power < 1:
                mouse_pos = vector2(pygame.mouse.get_pos())
                #print(mouse_pos.hat())
                pygame.draw.line(screen, BLACK, golfBall.pos, pygame.mouse.get_pos(), 1)
                power = myFont.render('Power: ' + str(int(percentage_power * 100)), False, (0,0,0))
            else:
                power = myFont.render('Power: 100', False, (0,0,0))
                pygame.draw.line(screen, BLACK, golfBall.pos, mouse_pos, 1)
        else:
            mouse_pos = golfBall.pos
        
        screen.blit(power, (600, 5))
        
        
        pygame.display.update()
        
        # --- Limit to 60 frames per second
        clock.tick(60)
        
    pygame.quit()
 
""" Safe start """
if __name__ == "__main__":
    try:
        main()
    except:
        pygame.quit()
        raise

