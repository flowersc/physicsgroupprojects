# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 14:41:13 2019

@author: Chris Flowers and Joey Burss
"""

import pygame as pygame

#constant colors
WHITE = ([255,255,255])
BLACK = ([0,0,0])
YELLOW = ([255,255,0])
BLUE = ([0,0,255])
GREEN = ([0,255,00])
AREA = ([190,190])
PURPLE = ([255, 0, 255])

done = False
turn = 0
whoWon = 0
zoneList = set()
listCount = 0

playing = True
        
def play():
    global done, turn, zoneList, myFont, listCount
    
    def createGameBoard(): 
        global topLeft, topMid, topRight, midLeft, midMid, midRight, bottomLeft, bottomMid, bottomRight, myFont
        #fills the black square onto the screen.
        screen.fill(WHITE)
        
        #This is the Black rectangle in the background
        pygame.draw.rect(screen, BLACK, (100,100,612,600))
        
        #This is the first row of white rectangles
        topLeft = pygame.draw.rect(screen, WHITE, (100, 100, 190, 190))
        topMid = pygame.draw.rect(screen, WHITE, (310, 100, 190, 190))
        topRight = pygame.draw.rect(screen, WHITE, (520, 100, 190, 190))
            
        #This is the secong row of white rectangles
        midLeft = pygame.draw.rect(screen, WHITE, (100, 310, 190, 190))
        midMid = pygame.draw.rect(screen, WHITE, (310, 310, 190, 190))
        midRight = pygame.draw.rect(screen, WHITE, (520, 310, 190, 190))
            
        #this is the third row of white rectangles
        bottomLeft = pygame.draw.rect(screen, WHITE, (100, 520, 190, 190))
        bottomMid = pygame.draw.rect(screen, WHITE, (310, 520, 190, 190))
        bottomRight = pygame.draw.rect(screen, WHITE, (520, 520, 190, 190))
        
        playerTurn = myFont.render('Players Turn:', False, (0,0,0))
        screen.blit(playerTurn, (600, -5))
        
    
    def xWon():
        global turn, whoWon, playing, listCount
        whoWon = 2
        turn = 2
        playing = False
        listCount = 0
        
    def oWon():
        global turn, whoWon, playing, listCount
        whoWon = 1
        turn = 2
        playing = False
        listCount = 0
        
    def tie():
        global myFont, playing, turn
        tieGame = myFont.render('Tie Game!', False, (0,0,0))
        screen.blit(tieGame, (360, 0))
        playAgain()
        playing = False
        turn = 2
        
        
        
    def checkWin(list):
        global turn, whoWon, playing
        
        #row check for win
        if (1 in list) and (2 in list) and (3 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (100, 195), (710, 195), 5)
            oWon()
        
        if (4 in list) and (5 in list) and (6 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (100, 405), (710, 405), 5)
            oWon()
            
        if (7 in list) and (8 in list) and (9 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (100, 615), (710, 615), 5)
            oWon()
        
        if (-1 in list) and (-2 in list) and (-3 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (100, 195), (710, 195), 5)
            xWon()
            
        if (-4 in list) and (-5 in list) and (-6 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (100, 405), (710, 405), 5)
            xWon()
            
        if (-7 in list) and (-8 in list) and (-9 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (100, 615), (710, 615), 5)
            xWon()
            
        
        #column check for win
        if (1 in list) and (4 in list) and (7 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (195, 100), (195, 710), 5)
            oWon()
        
        if (2 in list) and (5 in list) and (8 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (405, 100), (405, 710), 5)
            oWon()
            
        if (3 in list) and (6 in list) and (9 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (615, 100), (615, 710), 5)
            oWon()
        
        if (-1 in list) and (-4 in list) and (-7 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (195, 100), (195, 710), 5)
            xWon()
            
        if (-2 in list) and (-5 in list) and (-8 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (405, 100), (405, 710), 5)
            xWon()
            
        if (-3 in list) and (-6 in list) and (-9 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (615, 100), (615, 710), 5)
            xWon()
        
            
        
        
        #diagonal check for win
        if (1 in list) and (5 in list) and (9 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (100, 100), (710, 710), 5)
            oWon()
            
        if (3 in list) and (5 in list) and (7 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (710, 100), (100, 710), 5)
            oWon()
        
        if (-1 in list) and (-5 in list) and (-9 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (100, 100), (710, 710), 5)
            xWon()
           
        if (-3 in list) and (-5 in list) and (-7 in list):
            #display win text.
            #draw purple line across the 3 grids.
            pygame.draw.line(screen, PURPLE, (710, 100), (100, 710), 5)
            xWon()
            
        if listCount == 9:
            if whoWon == 0:
                tie()
            
        
        
        
    #try to implement this to clean up the type.event code in main.
    def checkForDraw():
        global turn, zoneList, playing, listCount
        #put the code from the mouse down event in here.
        if playing:
                if topLeft.collidepoint(pygame.mouse.get_pos()):
                    #print("Should have drawn a circle.")
                    if (1 in zoneList) or (-1 in zoneList):
                        #dont do anything
                        print("don't do anything.")
                    else:
                        if turn == 0:
                            zoneList.add(1)
                            turn = 1
                            listCount += 1
                        else:
                            zoneList.add(-1)
                            turn = 0
                            listCount += 1
                            
                if topMid.collidepoint(pygame.mouse.get_pos()):
                    #print("Should have drawn a circle.")
                    if (2 in zoneList) or (-2 in zoneList):
                        print("dont do antything.")
                    else:
                        if turn == 0:
                            zoneList.add(2)
                            turn = 1
                            listCount += 1
                        else:
                            zoneList.add(-2)
                            turn = 0
                            listCount += 1
                            
                if topRight.collidepoint(pygame.mouse.get_pos()):
                    #print("Should have drawn a circle.")
                    if (3 in zoneList) or (-3 in zoneList):
                        print("dont do antything.")
                    else:
                        if turn == 0:
                            zoneList.add(3)
                            turn = 1
                            listCount += 1
                        else:
                            zoneList.add(-3)
                            turn = 0
                            listCount += 1
                            
                if midLeft.collidepoint(pygame.mouse.get_pos()):
                    #print("Should have drawn a circle.")
                    if (4 in zoneList) or (-4 in zoneList):
                        print("dont do antything.")
                    else:
                        if turn == 0:
                            zoneList.add(4)
                            turn = 1
                            listCount += 1
                        else:
                            zoneList.add(-4)
                            turn = 0
                            listCount += 1
                            
                if midMid.collidepoint(pygame.mouse.get_pos()):
                    #print("Should have drawn a circle.")
                    if (5 in zoneList) or (-5 in zoneList):
                        print("dont do antything.")
                    else:
                        if turn == 0:
                            zoneList.add(5)
                            turn = 1
                            listCount += 1
                        else:
                            zoneList.add(-5)
                            turn = 0
                            listCount += 1
                            
                if midRight.collidepoint(pygame.mouse.get_pos()):
                    #print("Should have drawn a circle.")
                    if (6 in zoneList) or (-6 in zoneList):
                        print("dont do antything.")
                    else:
                        if turn == 0:
                            zoneList.add(6)
                            turn = 1
                            listCount += 1
                        else:
                            zoneList.add(-6)
                            turn = 0
                            listCount += 1
                            
                if bottomLeft.collidepoint(pygame.mouse.get_pos()):
                    #print("Should have drawn a circle.")
                    if (7 in zoneList) or (-7 in zoneList):
                        print("dont do antything.")
                    else:
                        if turn == 0:
                            zoneList.add(7)
                            turn = 1
                            listCount += 1
                        else:
                            zoneList.add(-7)
                            turn = 0
                            listCount += 1
                if bottomMid.collidepoint(pygame.mouse.get_pos()):
                    #print("Should have drawn a circle.")
                    if (8 in zoneList) or (-8 in zoneList):
                        print("dont do antything.")
                    else:
                        if turn == 0:
                            zoneList.add(8)
                            turn = 1
                            listCount += 1
                        else:
                            zoneList.add(-8)
                            turn = 0
                            listCount += 1
                if bottomRight.collidepoint(pygame.mouse.get_pos()):
                    #print("Should have drawn a circle.")
                    if (9 in zoneList) or (-9 in zoneList):
                        print("dont do antything.")
                    else:
                        if turn == 0:
                            zoneList.add(9)
                            turn = 1
                            listCount += 1
                        else:
                            zoneList.add(-9)
                            turn = 0
                            listCount += 1
        else:
            print("Game over... no more playing.")
        
    def manageBoard(list):
        global zoneList
        cirRad = 90
        if 1 in list:
            pygame.draw.circle(screen, GREEN, (195, 195), cirRad, 2)
        if -1 in list:
            pygame.draw.line(screen, BLUE, (100,100), (290,290), 3)
            pygame.draw.line(screen, BLUE, (100, 290), (290, 100), 3)
        if 2 in list:
            pygame.draw.circle(screen, GREEN, (405, 195), cirRad, 2)
        if -2 in list:
            pygame.draw.line(screen, BLUE, (310, 100), (500, 290), 3)
            pygame.draw.line(screen, BLUE, (310, 290), (500, 100), 3)
        if 3 in list:
            pygame.draw.circle(screen, GREEN, (615, 195), cirRad, 2)
        if -3 in list:
            pygame.draw.line(screen, BLUE, (520, 100), (710, 290), 3)
            pygame.draw.line(screen, BLUE, (520, 290), (710, 100), 3)
        if 4 in list:
            pygame.draw.circle(screen, GREEN, (195, 405), cirRad, 2)
        if -4 in list:
            pygame.draw.line(screen, BLUE, (100, 310), (290, 500), 3)
            pygame.draw.line(screen, BLUE, (100, 500), (290, 310), 3)
        if 5 in list:
            pygame.draw.circle(screen, GREEN, (405, 405), cirRad, 2)
        if -5 in list:
            pygame.draw.line(screen, BLUE, (310, 310), (500, 500), 3)
            pygame.draw.line(screen, BLUE, (310, 500), (500, 310), 3)
        if 6 in list:
            pygame.draw.circle(screen, GREEN, (615, 405), cirRad, 2)
        if -6 in list:
            pygame.draw.line(screen, BLUE, (520, 310), (710, 505), 3)
            pygame.draw.line(screen, BLUE, (520, 505), (710, 310), 3)
        if 7 in list:
            pygame.draw.circle(screen, GREEN, (195, 615), cirRad, 2)
        if -7 in list:
            pygame.draw.line(screen, BLUE, (100, 520), (295, 710), 3)
            pygame.draw.line(screen, BLUE, (100, 710), (295, 520), 3)
        if 8 in list:
            pygame.draw.circle(screen, GREEN, (405, 615), cirRad, 2)
        if -8 in list:
            pygame.draw.line(screen, BLUE, (310, 520), (505, 710), 3)
            pygame.draw.line(screen, BLUE, (310, 710), (505, 520), 3)
        if 9 in list:
            pygame.draw.circle(screen, GREEN, (615, 615), cirRad, 2)
        if -9 in list:
            pygame.draw.line(screen, BLUE, (520, 520), (710, 710), 3)
            pygame.draw.line(screen, BLUE, (520, 710), (710, 520), 3)
            
    def playAgain():
        playAgain = myFont.render('Play Again? Press R', False, (0,0,0))
        screen.blit(playAgain, (275, 30))
        
    def reset(list):
        global zoneList, turn, whoWon, playing, listCount
        list.clear()
        turn = 0
        whoWon = 0
        playing = True
        listCount = 0
                
    def myTurn():
                
        if turn == 0:
            #O's turn
            #draw over X with the same X just make it white
            #draw circle in upper right leaving room to the left for the X.
            pygame.draw.line(screen, WHITE, (750, 40), (775, 80), 3)
            pygame.draw.line(screen, WHITE, (750, 80), (775, 40), 3)
            pygame.draw.circle(screen, GREEN, (700, 60), 25, 2)
           # print("O's turn")
            
        elif turn == 1:
            #X's turn
            #draw over O with same type of O just make it white
            #draw an X in the upper right to the right of the O.
            pygame.draw.circle(screen, WHITE, (700, 60), 25, 2)
            pygame.draw.line(screen, BLUE, (750, 40), (775, 80), 3)
            pygame.draw.line(screen, BLUE, (750, 80), (775, 40), 3)
            
        elif turn == 2:
            #print to the screen that someone won!
            if whoWon == 1:
                #O won
                oWon = myFont.render('O Wins!', False, (0,0,0))
                screen.blit(oWon, (360, 0))
                playAgain()
            if whoWon == 2:
                #xWon
                xWon = myFont.render('X Wins!', False, (0,0,0))
                screen.blit(xWon, (360, 0))
                playAgain()
            
            
            
    def highlight():
        global turn
        if topLeft.collidepoint(pygame.mouse.get_pos()):
            lightSquare = pygame.draw.rect(screen, YELLOW, (100, 100, 190, 190), 5)
        else:
            lightSquare = pygame.draw.rect(screen, WHITE, (100, 100, 190, 190), 5)
        #topmid highlight
        if topMid.collidepoint(pygame.mouse.get_pos()):
            lightSquare = pygame.draw.rect(screen, YELLOW, (310, 100, 190, 190), 5)
        else:
            lightSquare = pygame.draw.rect(screen, WHITE, (310, 100, 190, 190), 5)
        #topright highlight
        if topRight.collidepoint(pygame.mouse.get_pos()):
            lightSquare = pygame.draw.rect(screen, YELLOW, (520, 100, 190, 190), 5)
        else:
            lightSquare = pygame.draw.rect(screen, WHITE, (520, 100, 190, 190), 5)
        #midLeft highlight
        if midLeft.collidepoint(pygame.mouse.get_pos()):
            lightSquare = pygame.draw.rect(screen, YELLOW, (100, 310, 190, 190), 5)
        else:
            lightSquare = pygame.draw.rect(screen, WHITE, (100, 310, 190, 190), 5)
        #midMid highlight
        if midMid.collidepoint(pygame.mouse.get_pos()):
            lightSquare = pygame.draw.rect(screen, YELLOW, (310, 310, 190, 190), 5)
        else:
            lightSquare = pygame.draw.rect(screen, WHITE, (310, 310, 190, 190), 5)
        #midRight highlight
        if midRight.collidepoint(pygame.mouse.get_pos()):
            lightSquare = pygame.draw.rect(screen, YELLOW, (520, 310, 190, 190), 5)
        else:
            lightSquare = pygame.draw.rect(screen, WHITE, (520, 310, 190, 190), 5)
        #botLeft highlight
        if bottomLeft.collidepoint(pygame.mouse.get_pos()):
            lightSquare = pygame.draw.rect(screen, YELLOW, (100, 520, 190, 190), 5)
        else:
            lightSquare = pygame.draw.rect(screen, WHITE, (100, 520, 190, 190), 5)
        #botMid highlight
        if bottomMid.collidepoint(pygame.mouse.get_pos()):
            lightSquare = pygame.draw.rect(screen, YELLOW, (310, 520, 190, 190), 5)
        else:
            lightSquare = pygame.draw.rect(screen, WHITE, (310, 520, 190, 190), 5)
        #botRight highlight
        if bottomRight.collidepoint(pygame.mouse.get_pos()):
            lightSquare = pygame.draw.rect(screen, YELLOW, (520, 520, 190, 190), 5)
        else:
            lightSquare = pygame.draw.rect(screen, WHITE, (520, 520, 190, 190), 5)
            
    
    screen = pygame.display.set_mode([800,800])
    clock = pygame.time.Clock()
    createGameBoard()
            
            
            
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
        # if to check if the mouse x and y is colliding with any of the points of the rects.\
            checkForDraw()
        if event.type == pygame.KEYUP and event.key == pygame.K_r:
            if playing == False:
                reset(zoneList)
                
                    
            
                    
    myTurn()
    highlight()
    manageBoard(zoneList)
    checkWin(zoneList)
    
    
    pygame.display.flip()
    clock.tick(60)



def main():
    # have to specify global for a variable defined outside of the function
    global turn, done, myFont, playing
    
    pygame.init()
    pygame.font.init()
    myFont = pygame.font.SysFont('Comic Sans MS', 30)
    
    while not done:
        play()
        
    pygame.quit()
    
    
    
""" Safe start """
if __name__ == "__main__":
    try:
        main()
    except:
        pygame.quit()
        raise
